asgiref==3.5.2
Django==3.2
django-widget-tweaks==1.4.12
Pillow==9.3.0
pkg_resources==0.0.0
pytz==2022.6
sqlparse==0.4.3
