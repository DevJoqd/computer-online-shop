from django.core.exceptions import ValidationError
import json
from . import information

NORMAL_VALIDATION_MESSAGE = 'لطفا فیلد های مورد نیاز را پر کنید'
ADVANCED_VALIDATION_MESSAGE = 'لطفا فیلد های مورد نیاز را پر کنید. فیلدهای مورد نیاز: {}'

class NoramlFieldValidator:
    def __init__(self, information: dict):
        self.information = information
        
    def validator(self, value):
        required_fields = list(self.information.keys() - value.keys())
        if not required_fields:
            return value
        raise ValidationError(NORMAL_VALIDATION_MESSAGE)
    
    
class AdvancedFieldValidator:
    def __init__(self, information: dict, keys: list):
        self.information = information
        self.keys = keys
        
    def validator(self, value):
        required_fields = list(self.information.keys() - value.keys() - set(self.keys))
        # excepted_fields = [i for i in self.keys if i not in required_fields]
        if not required_fields:
            return value

        fields_names = [self.information[i].get('label') for i in required_fields]
        fields_names = ', '.join(fields_names)
        raise ValidationError(ADVANCED_VALIDATION_MESSAGE.format(fields_names))