from django.shortcuts import render
from django.views.generic import TemplateView, DetailView
from .models import Product, Category
from django.db.models import Count
# Create your views here.

class Home(TemplateView):
    template_name = 'home/index.html'

    def get_context_data(self, *args, **kwargs):
        kwargs = super().get_context_data(*args, **kwargs)
        kwargs['categories'] = Category.objects.order_by('position')
        offs = Product.objects.filter(status='p', off_price__isnull=False)
        offs_by_cat = offs.values().annotate(count=Count('category')).order_by()
        kwargs['offs'] = {'all': offs, 'categories': offs_by_cat}
        kwargs['laptop'] = Product.objects.filter(category__slug='laptop')
        kwargs['gpu'] = Product.objects.filter(category__slug='gpu')
        print(kwargs)
        return kwargs

class ListCategory(DetailView):
    template_name = 'home/category.html'