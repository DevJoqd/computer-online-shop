# Information of each field in product model
from django import forms
rgb_field = forms.NullBooleanField(required=True, label='rgb دارد')

CPU_INF = {
    'generation': {
        'label':'نسل',
        'field': forms.CharField(max_length=64, required=True, label='نسل')
        },
    'frequence': {
        'label': 'فرکانس',
        'field': forms.CharField(max_length=32, required=True, label='فرکانس',
            help_text='بر اساس مگاهرتز(mhz)')
        },
    'cache': {
        'label':'مقدار کش',
        'field': forms.CharField(max_length=32, required=True, label='مقدار حافظه کش',
            help_text='بر اساس مگابایت(mgb)')
        },
    'socket': {
        'label':'سوکت',
        'field': forms.CharField(max_length=32, label='سوکت پردازنده', required=False),
        },
    'cores': {
        'label':'هسته',
        'field': forms.IntegerField(min_value=1, max_value=1024, label='هسته ها', required=True)
        },
    'tdp': {
        'label':'توان مصرفی',
        'field': forms.CharField(label='توان مصرفی', required=False)
        },
}

CPU_NULLABLE_FIELDS = [
    'socket',
    'tdp',
]

GPU_INF = {
    'min_power_need': {
        'label': 'حداقل منبع تغذیه مورد نیاز',
        'field': forms.CharField(label='حداقل منبع تغذیه مورد نیاز', max_length=32, required=False,
            help_text='بر اساس وات(w)'),
        },
    'memory_type': {
        'field': forms.CharField(label='نوع حافظه', max_length=32, required=False),
        'label':'نوع حافظه',
        },
    'memory_size': {
        'field': forms.DecimalField(max_value=512, min_value=0.5, required=True, label='حجم حافظه',
            help_text='حجم حافظه کارت گرافیک'
            ),
        'label':'حجم حافظه'
        },
    'ports': {
        'field': forms.CharField(max_length=512, label='پورت ها', required=False,
            help_text='تعداد پورت های کارت گرافیک'
            ),
        'label':'پورت ها',
        },
}
GPU_NULLABLE_FIELDS = [
    'min_power_need',
    'memory_type',
    'ports',
]

RAM_INF = {
    'capocity': {
        'label':'ظرفیت کلی',
        'field': forms.CharField(max_length=64, required=True, label='ظرفیت کلی',
            help_text='حجم کلی حافظه رم'
            ),
        },
    'frequence': {
        'label': 'فرکانس',
        'field': forms.CharField(max_length=215, label='فرکانس', required=True,
            help_text='فرکانس یا باس رم'
            )
        },
    'ram_type': {
        'label':'نوع حافظه',
        'field': forms.CharField(max_length=64, required=True, label='نوع حافظه',
            help_text='مثال: DDR4'
            ),
        },
    'modules': {
        'label': 'تعداد ماژول ها',
        'field': forms.IntegerField(max_value=16, min_value=1, required=False, label='تعداد ماژول ها')
        },
    'each_modules': {
        'label': 'حجم هر ماژول',
        'field': forms.IntegerField(max_value=16, min_value=1, required=False, label='حجم هر ماژول')
        },
    'rgb': {
        'label': 'rgb دارد',
        'field': rgb_field,
        },
}

RAM_NULLABLE_FIELDS = [
    'modules',
    'each_modules',
]

MAIN_BOARD_INF = {
    'chipset': {
        'label': 'چیپست',
        'field': forms.CharField(max_length=32, required=True, label='چیپست')
        },
    'form_factor': {
        'label': 'فرم فاکتور',
        'field': forms.CharField(required=True, max_length=32, label='فرم فاکتور',
            help_text=''
            ),
        },
    'ram_slots': {
        'label': 'تعداد اسلات های رم',
        'field': forms.IntegerField(required=True, min_value=1, max_value=16, label='تعداد اسلات های رم',
            help_text='تعداد اسلات های موجود رم'
            ),
        },
    'pci_slots': {
        'label': 'تعداد اسلات های pci-e',
        'field': forms.IntegerField(required=True, max_value=16, label='تعداد اسلات های pci-e',
            help_text='تعداد اسلات های موجود PCI-E'
            ),
        },
    'cpu_socket':  {
        'label': 'سوکت پردازنده',
        'field': forms.CharField(required=True, max_length=32, label='سوکت پردازنده',
            help_text='سوکتی که مادربورد پشتیبانی میکند'
            ),
        },
    'ram_type':  {
        'label': 'نوع رم',
        'field': forms.CharField(required=True, max_length=16, label='نوع رم',
            help_text='نوع رم های پشتیبانی شده. مثال: DDR4'
            ),
        },
    'max_cpu':  {
        'label': 'پردازنده های پشتیبانی شده',
        'field': forms.CharField(required=True, max_length=512, label='پردازنده های پشتیبانی شده',
            help_text='پردازنده هایی که مادربورد میتواند از آن پشتبیانی کنذ'
            ),
        },
}

POWER_INF = {
    'output_power': {
        'label': 'توان خروجی',
        'field': forms.CharField(max_length=32, label='توان خروجی', required=True,
            help_text='توان خروجی بر اساس وات'
            ),
        },
    'connector': {
        'label': 'کانکتور مادربورد',
        'field': forms.CharField(max_length=32, label='کانکتور مادربورد', required=True,
            help_text='تعداد پین های کانکتور مادربورد '
            ),
        },
}

FAN_INF = {
    'fan_type': {
        'label': 'نوع خنک کننده',
        'field': forms.CharField(required=True, max_length=32, label='نوع خنک کننده',
            help_text='مثال: کولینگ یا آبی'
            ),
        },
    'socket': {
        'label': 'پشتیبانی از سوکت',
        'field': forms.CharField(required=True, max_length=32, label='پشتیبانی از سوکت',
            help_text='سوکت پردازنده که بر آن نصب میشوذ'
            ),
        },
    'size': {
        'label': 'ابعاد',
        'field': forms.CharField(required=True, max_length=256, label='ابعاد'),
        },
    'rgb': {
        'label': 'rgb دارد',
        'field': rgb_field,
        },
}

CASE_INF = {
    'fan_places': {
        'label': 'تعداد فن های قابل نصب',
        'field': forms.IntegerField(required=True, max_value=32, min_value=1, label='تعداد فن های قابل نصب'),
        },
    'fan_installed': {
        'label': 'تعداد فن های نصب شده',
        'field': forms.IntegerField(required=True, max_value=32, min_value=0, label='تعداد فن های نصب شده'),
        },
    'size': {
        'label': 'ابعاد',
        'field': forms.CharField(required=True, max_length=256, label='ابعاد'),
        },
    'form_factor': {
        'label': 'فرم فکتور',
        'field': forms.CharField(required=True, max_length=32, label='فرم فکتور',
            help_text='فرم فکتور پشتبیانی شده'
            ),
        },
}

DISK_INF = {
    'disk_type': {
        'label': 'نوع دیسک',
        'field': forms.CharField(required=True, max_length=32, label='نوع دیسک',
            help_text='ssd یا hdd یا دیسک نوری'
            ),
        },
    'transfer_speed': {
        'label': 'سرعت انتقال',
        'field': forms.IntegerField(required=True, max_value=100000, min_value=0, label='سرعت انتقال',
            help_text='بر اساس مگابایت بر ثانیه'
            ),
        },
    'made_for': {
        'label': 'ساخته شده برای',
        'field': forms.CharField(required=True, max_length=512, label='ساخته شده برای',
            help_text='ساخته شده برای لپتاپ یا کامپیوتر'
            ),
        },
    'capocity': {
        'label': 'حجم',
        'field': forms.CharField(required=False, max_length=32, label='حجم'),
        },
}

DISK_NULLABLE_FIELDS = [
    'capocity',
]

BATTERY_INF = {
    'type': {
        'label': 'نوع باتری',
        'field': forms.CharField(required=True, max_length=64, label='نوع باتری',
            help_text='مثال: قلمی, لپتاپ, کتابی و ...'
            ),
        },
    'charging': {
        'label': 'مقدار شارژدهی',
        'field': forms.CharField(required=True, max_length= 32, label='مقدار شارژدهی',
            help_text='مقدار زمان شارژدهی'
            ),
        },
    'voltage': {
        'label': 'ولتاژ ورودی',
        'field': forms.CharField(required=True, max_length=32, label='ولتاژ ورودی'),
        },
    'capocity': {
        'label': 'ظرفیت',
        'field': forms.CharField(required=True, max_length=32, label='ظرفیت',
            help_text='ظرفیت باتری بر اساس mhl یا میلی آمپر'
            ),
        },
    'made_for': {
        'label': 'مناسب برای',
        'field': forms.CharField(required=False, max_length=256, label='مناسب برای'),
        },
}
BATTERY_NULLABLE_FIELDS = [
    'made_for',
]

MONITOR_INF = {
    'panel': {
        'label': 'پنل',
        'field': forms.CharField(required=False, max_length=8, label='پنل'),
        },
    'resolution': {
        'label': 'وضوح تصویر',
        'field': forms.CharField(required=True, max_length=64, label='وضوح تصویر')
        },
    'refresh_rate': {
        'label': 'رفرش ریت',
        'field': forms.CharField(required=True, max_length=16, label='رفرش ریت',
            help_text='بر اساس hz یا هرتز'
            ),
        },
    'ports': {
        'label': 'پرت ها',
        'field': forms.CharField(required=False, max_length=256, label='پرت ها',
            help_text='پرت های خروجی'
            ),
        },
    'scale': {
        'label': 'کشیدگی',
        'field': forms.CharField(required=True, max_length=32, label='کشیدگی',
            help_text='مثال: 16:9 یا 4:3'
            ),
        },
    'response_time': {
        'label': 'زمان پاسخ',
        'field': forms.CharField(required=False, max_length=8, label='زمان پاسخ',
            help_text='بر اساس میلی ثانیه(ms)'
            ),
        },
    'size': {
        'label': 'اندازه',
        'field': forms.CharField(required=True, max_length=32, label='اندازه',
            help_text='بر اساس اینچ(inch)'
            ),
        },
}
MONITOR_NULLABLE_FIELDS = [
    'ports',
    'panel',
    'response_time',
]

WEBCAM_INF = {
    'reolution': {
        'label': 'وضوح وبکم',
        'field': forms.CharField(required=True, max_length=32, label='وضوح وبکم',
            help_text='وضوح (resolution) وبکم'
            ),
        },
    'video_fps': {
        'label': 'fps فیلم برداری',
        'field': forms.CharField(required=True, max_length=16, label='fps فیلم برداری'),
        },
    'quality': {
        'label': 'کیفیت وبکم',
        'field': forms.CharField(required=True, max_length=32, label='کیفیت وبکم',
            help_text='720p, 480p, FHD, HD ...'
            ),
        },
}

CONNECTION_TYPE = ((1, 'باسیم'), (2, 'بیسیم'),)
KEYBOARD_INF = {
    'type': {
        'label': 'نوع', #mechanicall or ...
        'field': forms.CharField(required=False, max_length=32, label='نوع',
            help_text='مکانیکال یا ...'
            ),
        },
    'connection_type': {
        'label': 'نوع اتصال', #wired or wireless
        'field': forms.ChoiceField(choices=CONNECTION_TYPE, required=True, label='نوع اتصال',
            help_text='بیسیم, باسیم, هردو'
            ),
        },
    'connector': {
        'label': 'نوع متصل کننده', #if wired this field be use
        'field': forms.CharField(required=False, max_length=64, label='نوع متصل کننده',
            help_text=''
            ),
        },
    'rgb': {
        'label': 'rgb دارد',
        'field': rgb_field,
        },
}
KEYBOARD_NULLABLE_FIELDS = [
    'type',
    'connector',
]

MOUSE_INF = {
    'key_numbers': {
        'label': 'تعداد کلیدها',
        'field': forms.IntegerField(required=True, max_value=32, min_value=2, label='تعداد کلیدها'),
        },
    'connection_type': {
        'label': 'نوع اتصال',
        'field': forms.ChoiceField(required=True, choices=CONNECTION_TYPE, label='نوع اتصال',
            help_text='بیسیم یا باسیم یا هردو'
            ),
        },
    'dpi': {
        'label': 'دقت',
        'field': forms.CharField(required=True, max_length=512, label='دقت',
            help_text='محدوده دقت(dpi)'
            ),
        },
    'rgb': {
        'label': 'rgb دارد',
        'field': rgb_field,
        },
}

PAD_INF = {
    'material': {
        'label': 'جنس پدماوس',
        'field': forms.CharField(required=True, max_length=64, label='جنس پدماوس'),
        },
    'size': {
        'label': 'اندازه',
        'field': forms.CharField(required=True, max_length=256, label='اندازه'),
        },
    'features': {
        'label': 'ویژگی ها',
        'field': forms.CharField(required=True, max_length=512, label='ویژگی ها')
        },
}

CABLE_INF = {
    'size': {
        'label': 'اندازه',
        'field': forms.DecimalField(required=True, min_value=0, max_value=200, label='اندازه',
            help_text='طول کابل بر اساس متر'
            ),
        },
    'type': {
        'label': 'نوع',
        'field': forms.CharField(required=True, max_length=64, label='نوع',
            help_text='hdmi, vga, dvi, lan ...'
            ),
        },
}

ASSEMBLED_PC_iNF = ['cpu', 'gpu', 'ram', 'battery', 'disk', 'monitor', 'webcam']
LAPTOP_INF = ['cpu', 'gpu', 'ram', 'main_board', 'power', 'case', 'fan', 'disk']
BOUNDLE_INF = ['keyboard', 'mouse']