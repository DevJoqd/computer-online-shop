import os
from django.db import models
from django.urls import reverse_lazy
from .validators import NoramlFieldValidator, AdvancedFieldValidator
from django.utils import timezone
from . import information
from django.forms import ValidationError
# Create your models here.

class Product(models.Model):
    STATUS_CHOICES = (
        ('p', 'منتشر شده'),
        ('d', 'پیش نویس')
    )
    
    name = models.CharField(verbose_name='نام', max_length=512)
    category = models.ForeignKey('Category', on_delete=models.SET_NULL, related_name='category', verbose_name='دسته بندی', null=True)
    price = models.PositiveIntegerField(verbose_name='قیمت')
    cpu = models.JSONField(
        null=True, 
        blank=True, 
        verbose_name='پردازنده',
        validators=[AdvancedFieldValidator(information.CPU_INF, information.CPU_NULLABLE_FIELDS).validator]
    )
    gpu = models.JSONField(
        null=True, 
        blank=True, 
        verbose_name='کارت گرافیک',
        validators=[AdvancedFieldValidator(information.GPU_INF, information.GPU_NULLABLE_FIELDS).validator]
    )
    ram = models.JSONField(
        null=True, 
        blank=True, 
        verbose_name='رم',
        validators=[AdvancedFieldValidator(information.RAM_INF, information.RAM_NULLABLE_FIELDS).validator]
    )
    main_board = models.JSONField(
        null=True, 
        blank=True, 
        verbose_name='مادربورد',
        validators=[NoramlFieldValidator(information.MAIN_BOARD_INF).validator]
    )
    power = models.JSONField(
        null=True,
        blank=True,
        verbose_name='پاور',
        validators=[NoramlFieldValidator(information.POWER_INF).validator]
    )
    fan = models.JSONField(
        null=True, 
        blank=True, 
        verbose_name='خنک کننده',
        validators=[NoramlFieldValidator(information.FAN_INF).validator]
    )
    case = models.JSONField(
        null=True, 
        blank=True, 
        verbose_name='پوسته کیس',
        validators=[NoramlFieldValidator(information.CASE_INF).validator]
    )
    disk = models.JSONField(
        null=True, 
        blank=True, 
        verbose_name='دیسک',
        validators=[AdvancedFieldValidator(information.DISK_INF, information.DISK_NULLABLE_FIELDS).validator]
    )
    battery = models.JSONField(
        null=True, 
        blank=True, 
        verbose_name='باطری',
        validators=[AdvancedFieldValidator(information.BATTERY_INF, information.BATTERY_NULLABLE_FIELDS).validator]
    )
    monitor = models.JSONField(
        null=True, 
        blank=True, 
        verbose_name='مانیتور',
        validators=[AdvancedFieldValidator(information.MONITOR_INF, information.MONITOR_NULLABLE_FIELDS).validator]
    )
    webcam = models.JSONField(
        null=True, 
        blank=True, 
        verbose_name='وبکم',
        validators=[NoramlFieldValidator(information.WEBCAM_INF).validator]
    )
    mouse = models.JSONField(
        null=True, 
        blank=True, 
        verbose_name='ماوس',
        validators=[NoramlFieldValidator(information.MOUSE_INF).validator]
    )
    keyboard = models.JSONField(
        null=True, 
        blank=True, 
        verbose_name='کیبورد',
        validators=[AdvancedFieldValidator(information.KEYBOARD_INF, information.KEYBOARD_NULLABLE_FIELDS).validator]
    )
    pad = models.JSONField(
        null=True, 
        blank=True, 
        verbose_name='پدماوس',
        validators=[NoramlFieldValidator(information.PAD_INF).validator]
    )
    cable = models.JSONField(
        null=True, 
        blank=True, 
        verbose_name='کابل',
        validators=[NoramlFieldValidator(information.CABLE_INF).validator]
    )
    sticker = models.JSONField(
        null=True, 
        blank=True, 
        verbose_name='برچسب'
        )
    weight = models.IntegerField(
        null=True,
        blank=True,
        verbose_name='وزن'
    )
    # comments = models.ManyToManyField('Comment',null=True, blank=True, verbose_name='نظرات')
    colors = models.JSONField('رنگ های موجود', max_length=50)
    flags = models.JSONField()
    slug = models.SlugField(verbose_name='اسلاگ', unique=True)
    explain = models.TextField(verbose_name='توضیحات بیشتر')
    quantity = models.IntegerField(verbose_name='موجودی')
    created = models.DateTimeField(verbose_name='زمان ساخت', auto_now_add=True)
    off_price = models.PositiveIntegerField(verbose_name='مبلغ تخفیف', help_text='مبلغ تخفیفی که میخواهید بر روی کالا اعمال شود.', null=True, blank=True)
    status = models.CharField(default='d', verbose_name='وضعیت', choices=STATUS_CHOICES, max_length=20)
    publish = models.DateTimeField(verbose_name='زمان انتشار', null=True, blank=True)
    
    @property
    def display_price(self,):
        return f'{self.price:,}'
    
    class Meta:
        ordering = ('-publish',)
        verbose_name = 'محصول'
        verbose_name_plural = 'محصولات'
    
    def save(self, *args, **kwargs):
        if self.status == 'p':
            self.publish = timezone.now()
        super().save(*args, **kwargs)
    
    def __str__(self):
        return self.name
    
    def get_absolute_url(self):
        return reverse_lazy('account:products')

    
class Category(models.Model):
    name = models.CharField(max_length=256, verbose_name='نام')
    position = models.IntegerField(verbose_name='مکان قرارگیری')
    slug = models.SlugField(unique=True, verbose_name='اسلاگ')
    image = models.ImageField(verbose_name='پیش نمایش دسته بندی', upload_to='media/categories/')
    
    class Meta:
        ordering = ('position',)
        verbose_name = 'دسته بندی'
        verbose_name_plural = 'دسته بندی ها'
        
    def __str__(self):
        return self.name

    def save(self, *args,**kwargs):
        try:
            _object = Category.objects.get(id=self.pk)
            if _object.image and (_object.image != self.image):
                os.remove(_object.image.path)
        except Category.DoesNotExist :
            pass
        return super().save(*args, **kwargs)

    

class ProductItem(models.Model):
    item = models.ForeignKey(Product(), on_delete=models.CASCADE)
    quantity = models.PositiveSmallIntegerField()
    colors = models.JSONField()    

    def clean(self):
        for i in self.colors:
            if i not in self.item.colors.keys():
                raise ValidationError('color id not in exist on colors product!')
        super().clean()

    def __str__(self):
        return self.item.name