from django.urls import path, include
from .views import category as category_views,\
    product as product_views,\
    main as main_views

app_name = 'account'

urlpatterns = [
    path('', main_views.AdminDashboard.as_view(), name='dashboard'),
    
    path('products/', include([
        path('', product_views.AllProducts.as_view(), name='products'),
        path('add/', include([
            path('', main_views.ProductQuickAccess.as_view(), name='products_quick'),
            path('cpu/', product_views.AddCpuProduct.as_view(), name='add_cpu'),
            path('gpu/', product_views.AddGpuProduct.as_view(), name='add_gpu'),
            path('ram/', product_views.AddRamProduct.as_view(), name='add_ram'),
            path('mainboard/', product_views.AddMainboardProduct.as_view(), name='add_mainboard'),
            path('power/', product_views.AddPowerProduct.as_view(), name='add_power'),
            path('fan/', product_views.AddFanProduct.as_view(), name='add_fan'),
            path('case/', product_views.AddCaseProduct.as_view(), name='add_case'),
            path('disk/', product_views.AddDiskProduct.as_view(), name='add_disk'),
            path('battery/', product_views.AddBatteryProduct.as_view(), name='add_battery'),
            path('monitor/', product_views.AddMonitorProduct.as_view(), name='add_monitor'),
            path('webcam/', product_views.AddWebcamProduct.as_view(), name='add_webcam'),
            path('keyboard/', product_views.AddKeyboardProduct.as_view(), name='add_keyboard'),
            path('mouse/', product_views.AddMouseProduct.as_view(), name='add_mouse'),
            path('pad/', product_views.AddPadProduct.as_view(), name='add_pad'),
            path('cable/', product_views.AddCableProduct.as_view(), name='add_cable'),
            path('laptop/', product_views.AddLaptopProduct.as_view(), name='add_laptop'),
            path('assembled-case/', product_views.AddAssembledcase.as_view(), name='add_sc'),
            path('mouse-keyboard-boundle/', product_views.AddBoundleMK.as_view(), name='add_boundle'),
            ])
        ),
        path('<slug:slug>/', include([
            path('update/', product_views.UpdateProduct.as_view(), name='delete_product'),
            path('delete/', product_views.DeleteProduct.as_view(), name='update_product'),
            ])
        ),
        ])
    ),
    
    path('categories/', include([
        path('', category_views.ListCategory.as_view(), name='category_list'),
        path('add/', category_views.AddCategory.as_view(), name='add_category'),
        path('<slug:slug>/', include([
            path('update/', category_views.UpdateCategory.as_view(), name='update_category'),
            path('remove/', category_views.RemoveCategory.as_view(), name='remove_category'),
            ])
        ),
        ])
    ),
]