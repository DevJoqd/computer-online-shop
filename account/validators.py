from django.forms import ValidationError
import re

def phone_validator(phone: str):
    if phone.isdecimal():
        raise ValidationError('شماره وارد شد دارای کارکتر غیر مجاز می باشد. لطفا فقط از اعداد 0-9 استفاده کنید.')
    phone_regex = r'^[\d]{0,2}[09]+[0-9]{9}'
    if re.fullmatch(phone_regex, phone):
        return phone
    return ValidationError('لطفا شماره تلفن صحیح وارد کنید. به مانند: 09123456789')