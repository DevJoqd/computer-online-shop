from django import template
from web.models import Category

register = template.Library()


@register.filter
def check_start_url(url):
    return True if url.startswith('admin-panel/products/add/') else False

@register.inclusion_tag('home/hoverable_cat.html')
def categories_in_header():
    return {'cat_header': Category.objects.all()}