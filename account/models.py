from django.db import models
from django.contrib.auth.models import AbstractUser
from .validators import phone_validator
from web.models import ProductItem
from django.forms import ValidationError
# Create your models here.


class User(AbstractUser):
    phone = models.CharField(unique=True, max_length=11, verbose_name='شماره تلفن', validators=[phone_validator,])
    # otp = models.ManyToManyField('')
    customer = models.OneToOneField("account.Customers", verbose_name='اطلاعات کاربر', on_delete=models.CASCADE, null=True)


class Customers(models.Model):
    full_name = models.CharField('نام کامل', max_length=128)
    email = models.EmailField('ایمیل', blank=True, null=True)
    addresses = models.ManyToManyField('Address', verbose_name='آدرس ها')
    cart = models.ForeignKey('account.Cart', verbose_name='سبد خرید', on_delete=models.CASCADE)
    sales_order = models.ManyToManyField('account.SalesOrder', verbose_name='سفارشات')
    
    class Meta:
        verbose_name = 'خریدار'
        verbose_name_plural = 'خریداران'
    
# class Address(models.Model):

class Cart(models.Model):
    PAID_STATUS = (
        ('p', 'پرداخت شده'),
        ('np', 'پرداخت نشده')
    )
    product_item = models.ManyToManyField(ProductItem, blank=True)
    total_price = models.PositiveBigIntegerField(verbose_name='ارزش کل', null=True, blank=True)
    paid_status = models.CharField('وضعیت پرداخت', max_length=50, choices=PAID_STATUS)
    
    # This function must be in view not here ##
    # def save(self, *args, **kwargs):
    #     price = 0
    #     for i in self.product_item:
    #         price += i.item.price
    #     if self.total_price != price:
    #         self.total_price = price
    #     super().save(*args, **kwargs)
    
    @property
    def display_price(self,):
        return f'{self.total_price:,}'
    
    
class SalesOrder(models.Model):
    ORDER_STATUS = (
        ('p', 'در حال پردازش'),
        ('s', 'ارسال شده'),
        ('r', 'دریافت شده'),
        ('b', 'برگشت خورده')
    )
    paid_date = models.DateTimeField(verbose_name='زمان پرداخت', auto_now=False, auto_now_add=False)
    order_status = models.CharField(verbose_name='وضعیت سفارش', max_length=50, choices=ORDER_STATUS)
    factor = models.JSONField(verbose_name='فاکتور')
    items = models.ManyToManyField(ProductItem, verbose_name='آیتم ها')
    total_price = models.PositiveBigIntegerField(verbose_name='ارزش کل')
    
    @property
    def display_price(self,):
        return f'{self.total_price:,}'
    
    
class Address(models.Model):
    province = models.ForeignKey('account.Provinces', verbose_name='استان', on_delete=models.DO_NOTHING)
    cities = models.ForeignKey('account.Cities', verbose_name='شهر', on_delete=models.DO_NOTHING)
    address = models.CharField(verbose_name='آدرس', max_length=512)
    plaque = models.PositiveSmallIntegerField(verbose_name='پلاک')
    description = models.CharField(verbose_name='توضیحات', max_length=512, null=True, blank=True)

    def clean(self):
        if self.cities.province != self.province:
            raise ValidationError('شهر ورودی جز استان وارد شده نمی باشد!')
        return super().clean()

    def __str__(self):
        return self.id


class State(models.Model):
    name = models.CharField(max_length=32)
    slug = models.CharField(max_length=32)
    
    class Meta:
        abstract = True

    def __str__(self):
        return self.name    
    

class Provinces(State):
    class Meta:
        ordering = ('id',)
        
    
class Cities(State):
    province = models.ForeignKey(Provinces, on_delete=models.CASCADE)

    class Meta:
        ordering = ('province',)