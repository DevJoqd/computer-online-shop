from django.views.generic import TemplateView
from ..mixins import AdminsMixin, AdminPanelTemplateBase

class AdminDashboard(AdminPanelTemplateBase, AdminsMixin, TemplateView):
    template_name = 'index.html'    


class ProductQuickAccess(AdminsMixin, AdminPanelTemplateBase, TemplateView):
    template_name = 'quick-add-product.html'