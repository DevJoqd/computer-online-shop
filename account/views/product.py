from django.views.generic import CreateView, ListView, UpdateView, DeleteView
from ..mixins import CleanProductMixin, AdminsMixin, AdminPanelTemplateBase, GetObjectProductMixin
from web.models import Product
from .. import forms 
from web import information



class AddCpuProduct(CleanProductMixin, CreateView):
    product_name = 'cpu'
    label_product = 'پردازنده'
    
    # def form_valid(self, form):
    #     _output_form = form.output()
    #     form = form.save(commit=False)
    #     form.cpu = _output_form
    #     messages.success(self.request, f'محصول {form.name} با موفقیت اضافه شد!')
    #     return super().form_valid(form)


class AddGpuProduct(CleanProductMixin, CreateView):
    product_name = 'gpu'
    label_product = 'کارت گرافیک'



class AddRamProduct(CleanProductMixin, CreateView):
    product_name = 'ram'
    label_product = 'رم'


class AddMainboardProduct(CleanProductMixin, CreateView):
    product_name = 'main_board'
    label_product = 'مادربورد'


class AddPowerProduct(CleanProductMixin, CreateView):
    product_name = 'power'
    label_product = 'منبع تغذیه'


class AddFanProduct(CleanProductMixin, CreateView):
    product_name = 'fan'
    label_product = 'خنک کننده'


class AddCaseProduct(CleanProductMixin, CreateView):
    product_name = 'case'
    label_product = 'قاب کیس'


class AddDiskProduct(CleanProductMixin, CreateView):
    product_name = 'disk'
    label_product = 'دیسک'


class AddBatteryProduct(CleanProductMixin, CreateView):
    product_name = 'battery'
    label_product = 'باطری'


class AddMonitorProduct(CleanProductMixin, CreateView):
    product_name = 'monitor'
    label_product = 'مانیتور'


class AddWebcamProduct(CleanProductMixin, CreateView):
    product_name = 'webcam'
    label_product = 'وبکم'


class AddKeyboardProduct(CleanProductMixin, CreateView):
    product_name = 'keyboard'
    label_product = 'کیبورد'


class AddMouseProduct(CleanProductMixin, CreateView):
    product_name = 'mouse'
    label_product = 'ماوس'


class AddPadProduct(CleanProductMixin, CreateView):
    product_name = 'pad'
    label_product = 'پدماوس'


class AddCableProduct(CleanProductMixin, CreateView):
    product_name = 'cable'
    label_product = 'کابل'


class AddLaptopProduct(CleanProductMixin, CreateView):
    items = ['cpu', 'gpu', 'ram', 'battery', 'disk', 'monitor', 'webcam']
    items = information.LAPTOP_INF
    type_product = 'g'
    label_product = 'لپ تاپ'


class AddAssembledcase(CleanProductMixin, CreateView):
    type_product = 'g'
    label_product = 'کیس های اسمبل شده'
    items = information.ASSEMBLED_PC_iNF


class AddBoundleMK(CleanProductMixin, CreateView):
    type_product = 'g'
    label_product = 'باندل کیبورد و ماوس'
    items = information.BOUNDLE_INF


class AllProducts(AdminsMixin, AdminPanelTemplateBase, ListView):
    model = Product
    template_name = 'list-products.html'
    context_object_name = 'data'
    def get_paginate_by(self, queryset):
        page = self.request.GET.get('page')
        return page if page else 10


class UpdateProduct(AdminsMixin, AdminPanelTemplateBase, GetObjectProductMixin, UpdateView):
    template_name = 'update-product.html'
    form_class = forms.UpdateProduct

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['flags'] = list(self.get_object().flags)
        return kwargs


class DeleteProduct(AdminsMixin, AdminPanelTemplateBase, GetObjectProductMixin, DeleteView):
    template_name = 'delete-product.html'