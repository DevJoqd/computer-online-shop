from ..mixins import AdminsMixin, AdminPanelTemplateBase, SuccessProccessMixin, AddModelNameTemplateMixin
from django.views import generic
from django.shortcuts import get_object_or_404
from django.contrib import messages
from django.urls import reverse_lazy
from web.models import Category
from .. import forms

class ListCategory(AdminsMixin, AdminPanelTemplateBase, generic.ListView):
    model = Category
    template_name = 'category/list-category.html'
    paginate_by = 15
    context_object_name = 'data'


class AddCategory(AdminsMixin, AdminPanelTemplateBase,
                SuccessProccessMixin, AddModelNameTemplateMixin,
                generic.CreateView):
    model = Category
    form_class = forms.CategoryForm
    template_name = 'add-item-product.html'
    success_message = 'افزوده شد'
    def get_success_url(self):
        return reverse_lazy('account:category_list')


class UpdateCategory(AdminsMixin, AdminPanelTemplateBase,
                    SuccessProccessMixin, AddModelNameTemplateMixin, 
                    generic.UpdateView):
    model = Category
    form_class = forms.CategoryForm
    template_name = 'category/update-category.html'
    success_message = 'به روزرسانی شد'

    def get_success_url(self):
        return reverse_lazy('account:category_list')
    
    def get_object(self):
        slug = self.kwargs.get(self.slug_url_kwarg)
        return get_object_or_404(Category, slug=slug)



class RemoveCategory(AdminsMixin, AdminPanelTemplateBase,
                     AddModelNameTemplateMixin, generic.DeleteView):
    model = Category
    template_name = 'category/delete-category.html'

    def get_object(self):
        slug = self.kwargs.get(self.slug_url_kwarg)
        return get_object_or_404(Category, slug=slug)
    
    def get_success_url(self):
        return reverse_lazy('account:category_list')
    
    def delete(self, request, *args, **kwargs):
        messages.success(request, f'دسته بندی {self.get_object().name} با موفقیت  حذف شد!')
        return super().delete(request, *args, **kwargs)
