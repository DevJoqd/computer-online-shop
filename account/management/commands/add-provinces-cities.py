from django.core.management.base import BaseCommand
# from django.db.models.loading import get_model
from django.apps import apps
import csv


class Command(BaseCommand):
    help = 'Add iran`s cities and provinces to db '

    def add_arguments(self, parser):
        parser.add_argument('--cities_path', type=str, help="path of cities file")
        parser.add_argument('--provinces_path', type=str, help="file path")
        parser.add_argument('--cities_model', type=str, help="model name of cities")
        parser.add_argument('--provinces_model', type=str, help="model name of provinces")
        parser.add_argument('--app_name', type=str, help="django app name that the model is connected to")

    def handle(self, *args, **options):
        app_name = options['app_name']
        self.add_to_db(app_name=app_name, model_name=options['provinces_model'], csv_path=options['provinces_path'])
        self.add_to_db(app_name=app_name, model_name=options['cities_model'], csv_path=options['cities_path'])

    def add_to_db(self, app_name, model_name, csv_path):
        _model = apps.get_model(app_name, model_name)
        
        with open(csv_path, 'r') as csv_file:
            reader = csv.reader(csv_file, delimiter=',', quotechar='|')
            # header = reader.next()
            print(reader)
            header = next(reader)
            for row in reader:
                _object_dict = {key: value for key, value in zip(header, row)}
                _model.objects.create(**_object_dict)