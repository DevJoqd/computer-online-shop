from django.http.response import HttpResponseRedirect
from django.contrib import messages
from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404
from .forms import ProductForm
from web.models import Product

class AdminsMixin:
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_superuser or request.user.is_staff:
            return super().dispatch(request, *args, **kwargs)
        else:
            messages.error(request, 'لطفا با حساب ادمین وارد شوید!')
            return HttpResponseRedirect(reverse_lazy('admin:login'))


class AdminPanelTemplateBase:
    def get_template_names(self):
        base_template = 'admin-panel/'
        return base_template + self.template_name


class SendTypeToFormMixin:
    items = []
    type_product = ''   # can be 'i' or 'g'
    def get_form_kwargs(self):
        kwarg = super().get_form_kwargs()
        kwarg['item_or_group'] = {'_type': self.type_product, 'items': self.get_items()}
        return kwarg


class AddTypeProductTemplate:
    def get_context_data(self, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs['item_label'] = self.label_product
        return kwargs


class CleanProductMixin(AdminPanelTemplateBase, SendTypeToFormMixin,
                    AddTypeProductTemplate, AdminsMixin):
    model = Product
    form_class = ProductForm
    template_name = 'add-item-product.html'
    type_product = 'i'
    items = []
    product_name = None     # :Str  this variable is exist that means we have a product not a group like laptop 
    label_product = None    # :Str Persian label for displaying on template

    def get_success_url(self):
        return reverse_lazy('account:products')

    def get_items(self):
        if self.items:
            return self.items
        return [self.product_name,] 

    def form_valid(self, form):
        _output_form = form.output()
        data = form.cleaned_data
        form = form.save(commit=False)
        form.colors = data['colors']
        for i in self.get_items():
            setattr(form, i, _output_form[i])
        form.flags = self.get_items()
        messages.success(self.request, f'محصول {form.name} با موفقیت اضافه شد!')
        return super().form_valid(form)


class GetObjectProductMixin:
    def get_object(self,):
        return get_object_or_404(Product, self.kwargs.get(self.slug_url_kwarg))
    
    
class SuccessProccessMixin:
    success_message = None
    def form_valid(self, form):
        form_name = form.cleaned_data['name']
        form = form.save(commit=False)
        _var = super().form_valid(form)
        messages.success(self.request, f'{self.model._meta.verbose_name.title()} {form_name} {self.success_message}!')
        return _var
    

class AddModelNameTemplateMixin:
    def get_context_data(self, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs['item_label'] = self.model._meta.verbose_name.title()
        return kwargs