from django import forms
from django.utils.text import slugify
from web.models import Product, Category
from web import information

FIELDS_IN_ALL = [
    'name',
    'category',
    'price',
    'quantity',
    'slug',
    'status',
    # 'off_price',
    'explain',
    'weight',
]

# class ProductCpuForm(forms.ModelForm):
#     def __init__(self, *args, **kwargs):
#         super().__init__(*args, **kwargs)

#     generation = forms.CharField(max_length=64, required=True, label='نسل')
#     frequence = forms.CharField(max_length=32, required=True, label='فرکانس')
#     cache = forms.CharField(max_length=32, required=True, label='مقدار حافظه کش')
#     socket = forms.CharField(max_length=32, label='سوکت پردازنده', required=False)
#     cores = forms.IntegerField(min_value=1, max_value=1024, label='هسته ها', required=True)
#     tdp = forms.CharField(label='توان مصرفی', required=False)
#     _type = 'item'

#     class Meta:
#         model = Product
#         fields = FIELDS_IN_ALL.append('cpu')

#     def validate_cpu(self):
#         _out = {}
#         for i in information.CPU_INF.keys():
#             _value = getattr(self, i)
#             if _value:
#                 _out[i] = _value
#             elif self._type == 'item' and i in information.CPU_NULLABLE_FIELDS:
#                 raise forms.ValidationError(f'فیلد {information.CPU_INF.get(i)} نباید خالی باشد!')

#         return _out


class ProductForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        item_group = kwargs.pop('item_or_group')
        super().__init__(*args, **kwargs)
        # Structre of item_or_group. it must be send from view
        # item_or_group = {'_type': 'i' or 'g', 'items': fields }}
        self.type = item_group.get('_type')
        self.became_fields = [(i.upper() + '_INF') for i in item_group.get('items')]

        if self.type == 'i':
            _field = getattr(information, self.became_fields[0])
            self.set_fields(_field)
        else:
            for i in self.became_fields:
                _field = getattr(information, i)
                self.set_fields(_field)

    def set_fields(self, _field):
        for _key, _value in _field.items():
            self.fields[_key] = _value.get('field')

    def output(self,):
        _out = {}
        for i in self.became_fields:
            original_name_item = i.rstrip('_INF').lower()
            fields = getattr(information, i).keys()
            _out[original_name_item] = {}
            for j in fields:
                data = self.cleaned_data.get(j)
                if data:
                    _out[original_name_item][j] = data            
        return _out
    
    class Meta:
        model = Product
        fields = FIELDS_IN_ALL

    colors = forms.CharField(max_length=1024, label='رنگ های موجود',
                             required=True, help_text='لطفا مجموعه رنگ های مورد نظر را  پشت سر هم بنویسید و با استفاده از علامت «,» آن ها را از یکدیگر جدا کنید.')
    
    def clean_colors(self):
        splited_data = str(self.cleaned_data['colors']).split(',')

        if len(splited_data) == 0:
            raise forms.ValidationError({'colors': 'این فیلد مورد نیاز است'})
        else:
            colors = []
            for i in splited_data:
                colors.append(slugify(i.strip(' '), allow_unicode=True))
            return colors
        
    def clean(self,):
        super().clean()
        self.cleaned_data['colors'] = self.clean_colors()
        return self.cleaned_data


class CategoryForm(forms.ModelForm):
    
    class Meta:
        model = Category
        fields = '__all__'


class UpdateProduct(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.flags = kwargs.pop('flags')
        for _field in self.flags:
            fields_getted = getattr(information, _field.upper() + '_INF')
            self.set_fields(fields_getted, _field)
        super().__init__(*args, **kwargs)
            
    def set_fields(self, _field, fieldname):
        for _key, _value in _field.items():
            self.fields[_key] = _value.get('field')
            self.fields[_key].initial = self.instance.get(fieldname).get(_key)

    class Meta:
        model = Product
        fields = FIELDS_IN_ALL