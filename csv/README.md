Thanks to sajaddp for list of cities and provinces.
https://github.com/sajaddp/list-of-cities-in-Iran#

For adding provinces and cities, run blow command. this cities and provinces is for better normalization in database.

python3 manage.py add-provinces-cities --cities_path ./csv/cities.csv --provinces_path ./csv/provinces.csv --cities_model Cities --provinces_model Provinces --app_name account